package cl.nttdata.user.req;

import lombok.Getter;
import lombok.Setter;

public class TelefonoDto {
	@Getter
	@Setter
	private String numero;
	@Getter
	@Setter
	private String codigoCiudad;
	@Getter
	@Setter
	private String codigoPais;

}
