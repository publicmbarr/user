package cl.nttdata.user.req;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class UsuarioRes {

	@Getter
	@Setter
	private String id;
	@Getter
	@Setter
	private LocalDate creado;
	@Getter
	@Setter
	private LocalDate modificado;
	@Getter
	@Setter
	private LocalDate ultimoLogin;
	@Getter
	@Setter
	private String token;
	@Getter
	@Setter
	private Boolean activo;

}
