package cl.nttdata.user.req;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class UsuarioReq {

	@Getter
	@Setter
	private String nombre;
	@Getter
	@Setter
	private String correo;
	@Getter
	@Setter
	private String contrasena;
	@Getter
	@Setter
	private List<TelefonoDto> telefonos;

}
