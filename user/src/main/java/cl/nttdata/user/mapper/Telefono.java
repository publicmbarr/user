package cl.nttdata.user.mapper;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table
public class Telefono {
	@Id
	@GeneratedValue
	@Getter
	private Long id;
	@Getter
	@Setter
	private String numero;
	@Getter
	@Setter
	private String codigoCiudad;
	@Getter
	@Setter
	private String codigoPais;


}
