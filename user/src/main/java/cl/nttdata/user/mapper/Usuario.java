package cl.nttdata.user.mapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @author mbarriga
 *
 */
@Entity
@Table
public class Usuario {

	@Id
	@GeneratedValue (strategy = GenerationType.UUID)
	@Getter
	private String id;
	@Getter
	@Setter
	@Column
	private String nombre;
	@Getter
	@Setter
	@Column(unique = true)
	private String correo;
	@Getter
	@Setter
	@Column
	private LocalDate creado;
	@Getter
	@Setter
	@Column
	private LocalDate modificado;
	@Getter
	@Setter
	@Column
	private LocalDate ultimoLogin;
	@Getter
	@Setter
	@Column
	private String token;
	@Getter
	@Setter
	@Column
	private String contrasena;
	@Getter
	@Setter
	@Column
	private boolean activo;
	@Getter
	@Setter
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "usuario_id")
	private List<Telefono> telefonos = new ArrayList<>();

}
