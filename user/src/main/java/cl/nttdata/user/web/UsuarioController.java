package cl.nttdata.user.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

import cl.nttdata.user.mapper.Usuario;
import cl.nttdata.user.req.UsuarioReq;
import cl.nttdata.user.req.UsuarioRes;
import cl.nttdata.user.service.IUsuarioService;

@RestController
@RequestMapping("/api")
public class UsuarioController {

	@Autowired	
	private IUsuarioService usuarioService;
	
	@GetMapping ("/usuarios")
	List<Usuario> all(){
		return usuarioService.findAll();
	}

	@GetMapping ("/usuarios/{id}")
	Usuario getOne(@PathVariable String id){
		return usuarioService.getOne(id);
	}

	@PostMapping ("/usuarios")
	UsuarioRes create(@RequestBody UsuarioReq usuarioRes) {
		return usuarioService.create(usuarioRes);
	}

	@PutMapping ("/usuarios/{id}")
	UsuarioRes update(@RequestBody UsuarioReq usuarioRes, @PathVariable String id) {
		return usuarioService.update(usuarioRes, id);
	}

	@DeleteMapping ("/usuarios/{id}")
	void delete(@PathVariable String id) {
		usuarioService.delete(id);
	}

	@PatchMapping(path = "/usuarios/{id}", consumes = "application/json-patch+json")
	public ResponseEntity<MappingJacksonValue> updateUsuario(@PathVariable String id,
	                                          @RequestBody JsonPatch jsonPatch) throws JsonProcessingException, IllegalArgumentException, JsonPatchException {
		return usuarioService.patchUsuario(jsonPatch, id);
	}
}
