package cl.nttdata.user.jwt;

import java.security.Key;
import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

/**
 * Generador de token jwt
 * 
 * @author mbarriga
 *
 */
public class JwtGenerator {

	private static final Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);

	
	/**
	 * Genera token
	 * @param subject usuario del token
	 * @param expirationMillis tiempo de vida del token
	 * @return
	 */
	public static String generateTokenJwt(String subject, long expirationMillis) {
		Date expirationDate = new Date(System.currentTimeMillis() + expirationMillis);
		String jwt = Jwts.builder().
				setHeaderParam("typ", "jwt").
				setSubject(subject).
				setExpiration(expirationDate).
				signWith(SECRET_KEY).
				setIssuer("mbarriga").
				setIssuedAt(new Date()).compact();
		return jwt;
	}
	
	
	/**
	 * Descompone token
	 * @param token
	 * @return
	 */
	public static String decodeTokenJwt(String token) {
			Jws<Claims> jwt = Jwts.parserBuilder()
				      .setSigningKey(SECRET_KEY)
				      .build()
				      .parseClaimsJws(token);
			return jwt.toString();
	}

}
