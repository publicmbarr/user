package cl.nttdata.user.exception;

public class UserRequestException extends RuntimeException {

	public UserRequestException(String message) {
		super(message);
	}
	
	public UserRequestException(String message, Exception e) {
		super(message,e);
	}

}
