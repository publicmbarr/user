package cl.nttdata.user.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Manejo de errores de sistema
 * @author mbarriga
 *
 */
@ControllerAdvice
public class RestErrorHandler extends ResponseEntityExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestErrorHandler.class);

	/**
	 * Errores de request
	 * @param ex
	 * @param request
	 * @return
	 */
	@ResponseBody
	@ExceptionHandler(UserRequestException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String handleUserRequestException(UserRequestException ex, WebRequest request) {
		logger.error(ex.getMessage());
		logger.error(request.getDescription(false));
		String bodyOfResponse = "{\"message\":\"" + ex.getMessage() + "\"}";
		return bodyOfResponse;
	}

	/**
	 * Errores de busqueda
	 * @param ex
	 * @param request
	 * @return
	 */
	@ResponseBody
	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
		logger.error(ex.getMessage());
		logger.error(request.getDescription(false));
		String bodyOfResponse = "{\"message\":\"" + ex.getMessage() + "\"}";
		return bodyOfResponse;
	}

}
