package cl.nttdata.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.nttdata.user.mapper.Usuario;


/**
 * @author mbarriga
 *
 */
public interface UsuarioRepository extends JpaRepository<Usuario, String> {
}
