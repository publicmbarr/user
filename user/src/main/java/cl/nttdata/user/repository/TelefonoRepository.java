package cl.nttdata.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.nttdata.user.mapper.Telefono;


/**
 * @author mbarriga
 *
 */
public interface TelefonoRepository extends JpaRepository<Telefono, Long> {
	
}
