package cl.nttdata.user.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

import cl.nttdata.user.exception.UserNotFoundException;
import cl.nttdata.user.exception.UserRequestException;
import cl.nttdata.user.jwt.JwtGenerator;
import cl.nttdata.user.mapper.Telefono;
import cl.nttdata.user.mapper.Usuario;
import cl.nttdata.user.repository.TelefonoRepository;
import cl.nttdata.user.repository.UsuarioRepository;
import cl.nttdata.user.req.TelefonoDto;
import cl.nttdata.user.req.UsuarioReq;
import cl.nttdata.user.req.UsuarioRes;

@PropertySource("classpath:format.properties")
@Service
public class UsuarioService implements IUsuarioService {

	private static final Logger logger = LoggerFactory.getLogger(UsuarioService.class);

	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private TelefonoRepository telefonoRepository;
	@Value("${formato.regex.contrasena}")
	private String formatoContrasena;
	@Value("${formato.regex.correo}")
	private String formatoEmail;

	private ObjectMapper objectMapper = new ObjectMapper();
	
	public UsuarioRes create(UsuarioReq usuarioReq) {
		logger.info("Crear Usuario");
		Usuario u = new Usuario();
		u.setCreado(LocalDate.now());
		u.setUltimoLogin(LocalDate.now());
		UsuarioRes usuarioRes = save(usuarioReq, u);
		return usuarioRes;
	}

	public UsuarioRes update(UsuarioReq usuarioReq, String id) {
		logger.info("Actualizar Usuario");
		Usuario u = getOne(id);
		if (!u.getTelefonos().isEmpty()) {
			for (Telefono t : u.getTelefonos()) {
				telefonoRepository.deleteById(t.getId());
			}
			u.getTelefonos().clear();
		}
		UsuarioRes usuarioRes = save(usuarioReq, u);
		return usuarioRes;
	}

	private UsuarioRes save(UsuarioReq usuarioReq, Usuario u) {
		if (Pattern.matches(formatoEmail, usuarioReq.getCorreo()))
			u.setCorreo(usuarioReq.getCorreo());
		else
			throw new UserRequestException("Error en formato de correo");
		
		if (Pattern.matches(formatoContrasena, usuarioReq.getContrasena())) {
			String base64 = Base64.getEncoder().encodeToString(usuarioReq.getContrasena().getBytes());
			u.setContrasena(base64);
		} else
			throw new UserRequestException("Error en formato de contraseña");

		u.setToken(JwtGenerator.generateTokenJwt(u.getCorreo(), 60 * 60 * 3600)); // genera token por 1 hora
		u.setNombre(usuarioReq.getNombre());
		u.setModificado(LocalDate.now());
		u.setActivo(true);

		List<Telefono> telefonos = new ArrayList<Telefono>();
		for (Iterator<TelefonoDto> iterator = usuarioReq.getTelefonos().iterator(); iterator.hasNext();) {
			TelefonoDto telefono = (TelefonoDto) iterator.next();
			Telefono t = new Telefono();
			t.setNumero(telefono.getNumero());
			t.setCodigoCiudad(telefono.getCodigoCiudad());
			t.setCodigoPais(telefono.getCodigoPais());
			telefonos.add(t);
		}
		u.getTelefonos().addAll(telefonos);
		try {
			u = usuarioRepository.save(u);
		} catch (DataIntegrityViolationException e) {
			throw new UserRequestException("El correo ya está registrado");
		}

		UsuarioRes usuarioRes = UsuarioRes.builder().id(u.getId()).creado(u.getCreado()).modificado(u.getModificado())
				.ultimoLogin(u.getUltimoLogin()).token(u.getToken()).activo(u.isActivo()).build();

		return usuarioRes;

	}

	@Override
	public List<Usuario> findAll() {
		logger.info("Listar Usuarios");
		List<Usuario> usuarios = usuarioRepository.findAll();
		return usuarios;
	}

	@Override
	public Usuario getOne(String id) {
		logger.info("Consultar Usuario");
		Optional<Usuario> u = usuarioRepository.findById(id);
		if (u.isPresent())
			return u.get();
		else
			throw new UserNotFoundException(id);
	}

	@Override
	public void delete(String id) {
		logger.info("Eliminar Usuario");
		usuarioRepository.deleteById(id);
	}
	
	@Override
	public ResponseEntity<MappingJacksonValue> patchUsuario(JsonPatch jsonPatch, String id) throws IllegalArgumentException, JsonPatchException, JsonProcessingException {
		logger.info("Actualizar Usuario");
		Usuario u = getOne(id);
		
		objectMapper.registerModule(new JavaTimeModule());
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("id","nombre");
		FilterProvider filters = new SimpleFilterProvider().addFilter("userFilter", filter);
		objectMapper.setFilterProvider(filters);
		JsonNode patched = jsonPatch.apply(objectMapper.convertValue(u, JsonNode.class));
		Usuario usuario= objectMapper.treeToValue(patched, Usuario.class);
		
		Usuario savedUser = usuarioRepository.save(usuario);
		MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(savedUser);
		mappingJacksonValue.setFilters(filters);
		return ResponseEntity.ok().body(mappingJacksonValue);
	}

}
