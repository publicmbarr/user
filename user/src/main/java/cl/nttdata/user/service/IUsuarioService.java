package cl.nttdata.user.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

import cl.nttdata.user.mapper.Usuario;
import cl.nttdata.user.req.UsuarioReq;
import cl.nttdata.user.req.UsuarioRes;

public interface IUsuarioService {

	public List<Usuario> findAll();
	public UsuarioRes create(UsuarioReq usuarioRes);
	public Usuario getOne(String id);
	public UsuarioRes update(UsuarioReq usuarioRes, String id);
	public void delete(String id);
	public ResponseEntity<MappingJacksonValue> patchUsuario(JsonPatch jsonPatch, String id) throws IllegalArgumentException, JsonPatchException, JsonProcessingException;

}
